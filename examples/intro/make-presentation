#!/usr/bin/python
from slides import Lecture, NumSlide, Slide, Bullet, SubBullet, PRE, URL


lecture = Lecture(
    "Slides: A Python Presentation Generator",
    Slide("Slides",
          Bullet("Generates HTML slides"),
          Bullet("Presentations are written in Python"),
          Bullet("Full power of Python available!"),
    ),
    Slide("Rejected alternatives - HTML",
          Bullet("Hard to change layout"),
          Bullet("too verbose"),
    ),
    Slide("Rejected alternatives - XML",
          Bullet("Need to write more code"),
          Bullet("Harder to extend"),
          Bullet("Need to learn new schema"),
          Bullet("XML -> object model -> HTML: why not skip a stage?"),
    ),
    Slide("Solution - Python",
          Bullet("Use Python objects for presentation elements"),
          Bullet("No need to learn new schema"),
          Bullet("Full power of programming language can be used"),
    ),
    Slide("An example",
          Bullet("An example presentation:", PRE("""
from slides import Lecture, Slide, Bullet

l = Lecture(
    Slide("This is a slide",
          Bullet("Bullet one"),
          Bullet("Another bullet"),
    ))
l.render("/tmp", "example-%d.html")
""")),
          Bullet("Next slide shows the output"),
    ),
    Slide("This is a slide",
          Bullet("Bullet one"),
          Bullet("Another bullet"),
    ),
    NumSlide("Fancy example",
          Bullet("Notice how we can do sublists:", SubBullet(
                 Bullet("A bullet"),
                 Bullet("Another one with a URL: ", URL("http://www.example.com")))),
          Bullet("Notice top level bullets can be ordered"),
    ),
    Slide("Available classes",
          Bullet("Lecture - contains Slides and renders them"),
          Bullet("Slide - a slide with title and list of bullets"),
          Bullet("Bullet - a bullet in a slide or SubBullet"),
          Bullet("SubBullet - a sublist of bullets"),
          Bullet("PRE, URL - text styles"),
    ),
    Slide("Extending is trivial",
          Bullet("Adding a bold style:", PRE("""
class Bold:
    def __init__(self, text):
        self.text = text
    def toHTML(self):
        return '<b>%s</b>' % slides.toHTML(self.text)

Bullet(Bold("some text")) # a bullet whose text is bold
"""))),
    Slide("Future work",
          Bullet("More built-in styles"),
          Bullet("Generates PDF or other formats"),
    ),
    Slide("Who uses it?",
          Bullet("Moshe Zadka - original author"),
          Bullet("Itamar Shtull-Trauring"),
          Bullet("You should too!"),
          Bullet("Download from ", URL("http://itamarst.org/software/")),
    ),
)

lecture.renderHTML(".", "slide-%d.html", css="main.css")
